#include <Arduino.h>
#include <Update.h>
#include "SPIFFS.h"
#include <NimBLEDevice.h>
#include <VescUart.h>
#include <esp_task_wdt.h>
#include <FastLED.h>
#include <SoftTimers.h>
#include <esp_task_wdt.h>

#define VERSION "v1.1.0"
#define WS_PIN 22
#define R_PIN 23
#define NUM_LEDS 45
#define LED_DEFAULT_BR 255
#define WDT_TIMEOUT 3
#define FULL_PACKET 520
#define FRAMES_PER_SECOND 30
#define REMOTE_SERVICE_UUID "A032F4C8-031A-41B3-A7DA-5090FC675923"
#define THROTTLE_CHARACTERISTIC_UUID "4EB96309-9129-4C6E-BAAA-AB23E5FD02BA"
#define LD_CHARACTERISTIC_UUID "61F4A76C-D46D-42FD-BD4D-1865217B2294"
#define LED_CHAR_UUID           "595C3D27-5D56-4EFD-8D44-FD611EA65AAA"
#define VESC_SERVICE_UUID            "6E400001-B5A3-F393-E0A9-E50E24DCCA9E"
#define VESC_CHARACTERISTIC_UUID_RX  "6E400002-B5A3-F393-E0A9-E50E24DCCA9E"
#define VESC_CHARACTERISTIC_UUID_TX  "6E400003-B5A3-F393-E0A9-E50E24DCCA9E"
#define OTA_SERVICE_UUID              "fb1e4001-54ae-4a28-9f74-dfccb248601d"
#define OTA_CHARACTERISTIC_UUID_RX    "fb1e4002-54ae-4a28-9f74-dfccb248601d"
#define OTA_CHARACTERISTIC_UUID_TX    "fb1e4003-54ae-4a28-9f74-dfccb248601d"
#define NORMAL_MODE   0   // normal
#define UPDATE_MODE   1   // receiving firmware
#define OTA_MODE      2   // installing firmware

uint8_t updater[16384];
uint8_t updater2[16384];

int MTU_SIZE = FULL_PACKET;
int BLE_PACKET_SIZE = MTU_SIZE - 3;
VescUart UART;
std::string bstr;

union packed_long {
  long l;
  uint8_t b[4];
};
union packed_float {
  float f;
  uint8_t b[4];
};


packed_float avgMCur, rpm, inpV;
packed_long tachAbs;
int i;
uint8_t r_data[16];

SoftTimer ldTimer;
SoftTimer ledTimer;

static NimBLECharacteristic* pCharOTATX;
static NimBLECharacteristic* pCharOTARX;
static NimBLECharacteristic* pCharThrottle;
static NimBLECharacteristic* pCharLD;
static NimBLECharacteristic* pCharLED;
static NimBLECharacteristic* pCharVESCTX;
static NimBLECharacteristic* pCharVESCRX;
static NimBLEServer* sServer;

static bool sendMode = false, sendSize = true;
static bool writeFile = false, request = false;
static int writeLen = 0, writeLen2 = 0;
static bool current = true, ledsOn = false;
static int parts = 0, next = 0, cur = 0, MTU = 0;
static int MODE = NORMAL_MODE;
unsigned long rParts, tParts;
uint8_t ledSt[3];

template <uint8_t DATA_PIN, EOrder RGB_ORDER = RGB> class WS2815Controller : public ClocklessController<DATA_PIN, C_NS(270), C_NS(340), C_NS(270), RGB_ORDER> {};
template <uint8_t DATA_PIN, EOrder RGB_ORDER> class WS2815 : public WS2815Controller<DATA_PIN, RGB_ORDER> {};

// Define the array of leds
CRGB leds[NUM_LEDS];
CRGBPalette16 gPal;
bool gReverseDirection = false;
// COOLING: How much does the air cool as it rises?
// Less cooling = taller flames.  More cooling = shorter flames.
// Default 55, suggested range 20-100 
#define COOLING  55

// SPARKING: What chance (out of 255) is there that a new spark will be lit?
// Higher chance = more roaring fire.  Lower chance = more flickery fire.
// Default 120, suggested range 50-200.
#define SPARKING 120



void updateLD(){
  if (UART.getVescValues()) {
    tachAbs.l = UART.data.tachometerAbs;
    inpV.f = UART.data.inpVoltage;
    avgMCur.f = UART.data.avgMotorCurrent;
    rpm.f = UART.data.rpm;
    for(i = 0; i < 4; i++){
      r_data[i] = rpm.b[i];
      r_data[i + 4] = inpV.b[i];
      r_data[i + 8] = avgMCur.b[i];
      r_data[i + 12] = tachAbs.b[i];
    }
    pCharLD->setValue(r_data, 16);
    pCharLD->notify();
  }
}

class ServerCallbacks: public NimBLEServerCallbacks {
    void onConnect(NimBLEServer* pServer, ble_gap_conn_desc* desc) {
        //Serial.println("Connected");
        /** We can use the connection handle here to ask for different connection parameters.
         *  Args: connection handle, min connection interval, max connection interval
         *  latency, supervision timeout.
         *  Units; Min/Max Intervals: 1.25 millisecond increments.
         *  Latency: number of intervals allowed to skip.
         *  Timeout: 10 millisecond increments, try for 5x interval time for best results.  
         */
        //pServer->updateConnParams(desc->conn_handle, 5, 5, 0, 30);
        NimBLEDevice::startAdvertising();
    }
    void onDisconnect(NimBLEServer* pServer) {
        UART.nunchuck.valueY = 128;
        UART.nunchuck.lowerButton = false;
        UART.setNunchuckValues();
    }
};


class MyCallbacks: public NimBLECharacteristicCallbacks {
    void onWrite(NimBLECharacteristic* pCharacteristic) {
        std::string rxValue = pCharacteristic->getValue();
        if (rxValue.length() > 0){
            if (pCharacteristic->getUUID().equals(pCharVESCRX->getUUID())){
                for (int i = 0; i < rxValue.length(); i++) {
                    Serial2.write(rxValue[i]);
                }
            } else if (pCharacteristic->getUUID().equals(pCharThrottle->getUUID())) {
                uint8_t data = rxValue[0];
                uint8_t cruise = rxValue[1];
                if(cruise > 0) UART.nunchuck.lowerButton = true;
                else UART.nunchuck.lowerButton = false;
                if (data > 0) UART.nunchuck.valueY = data;          
                UART.setNunchuckValues();
            } else if (pCharacteristic->getUUID().equals(pCharLED->getUUID())){
                ledSt[0] = rxValue[0];
                ledSt[1] = rxValue[1];
                ledSt[2] = rxValue[2];
            }
        }
    };
    void onMTUChange(uint16_t MTU, ble_gap_conn_desc* desc) {
        MTU_SIZE = MTU;
        BLE_PACKET_SIZE = MTU_SIZE - 3;
    }
};


class OtaCallbacks: public NimBLECharacteristicCallbacks {
    void onWrite(NimBLECharacteristic *pCharacteristic) {
      std::string pData = pCharacteristic->getValue();
      int len = pData.length();
      if (pData[0] == 0xFB) {
        int pos = pData[1];
        for (int x = 0; x < len - 2; x++) {
          if (current) {
            updater[(pos * MTU) + x] = pData[x + 2];
          } else {
            updater2[(pos * MTU) + x] = pData[x + 2];
          }
        }
      } else if  (pData[0] == 0xFC) {
        if (current) {
          writeLen = (pData[1] * 256) + pData[2];
        } else {
          writeLen2 = (pData[1] * 256) + pData[2];
        }
        current = !current;
        cur = (pData[3] * 256) + pData[4];
        writeFile = true;
        if (cur < parts - 1) {
          request = true;
        }
      } else if (pData[0] == 0xFD) {
        sendMode = true;
        if (SPIFFS.exists("/update.bin")) {
          SPIFFS.remove("/update.bin");
        }
      } else if (pData[0] == 0xFE) {
        rParts = 0;
        tParts = (pData[1] * 256 * 256 * 256) + (pData[2] * 256 * 256) + (pData[3] * 256) + pData[4];
      } else if  (pData[0] == 0xFF) {
        parts = (pData[1] * 256) + pData[2];
        MTU = (pData[3] * 256) + pData[4];
        MODE = UPDATE_MODE;
      } else if (pData[0] == 0xEF) {
        SPIFFS.format();
        sendSize = true;
      }
    }
};


static void writeBinary(fs::FS &fs, const char * path, uint8_t *dat, int len) {


  File file = fs.open(path, FILE_APPEND);

  if (!file) {
    return;
  }
  file.write(dat, len);
  file.close();
  writeFile = false;
  rParts += len;
}

void sendOtaResult(String result) {
  byte arr[result.length()];
  result.getBytes(arr, result.length());
  pCharOTATX->setValue(arr, result.length());
  pCharOTATX->notify();
  delay(200);
}


void performUpdate(Stream &updateSource, size_t updateSize) {
  char s1 = 0x0F;
  String result = String(s1);
  if (Update.begin(updateSize)) {
    size_t written = Update.writeStream(updateSource);
    result += "Written : " + String(written) + "/" + String(updateSize) + " [" + String((written / updateSize) * 100) + "%] \n";
    if (Update.end()) {
      result += "OTA Done: ";
      if (Update.isFinished()) {
        result += "Success!\n";
      }
      else {
        result += "Failed!\n";
      }

    }
    else {
      result += "Error";
    }
  }
  else
  {
    result += "No space";
  }
  if (sServer->getConnectedCount()) {
    sendOtaResult(result);
    delay(1000);
  }
}

void updateFromFS() {
  File updateBin = SPIFFS.open("/update.bin");
  if (updateBin) {
    if (updateBin.isDirectory()) {
      updateBin.close();
      return;
    }

    size_t updateSize = updateBin.size();

    if (updateSize > 0) {
      performUpdate(updateBin, updateSize);
    }

    updateBin.close();

    SPIFFS.remove("/update.bin");

    ESP.restart();
  }
}


CRGBPalette16 pacifica_palette_1 = 
    { 0x000507, 0x000409, 0x00030B, 0x00030D, 0x000210, 0x000212, 0x000114, 0x000117, 
      0x000019, 0x00001C, 0x000026, 0x000031, 0x00003B, 0x000046, 0x14554B, 0x28AA50 };
CRGBPalette16 pacifica_palette_2 = 
    { 0x000507, 0x000409, 0x00030B, 0x00030D, 0x000210, 0x000212, 0x000114, 0x000117, 
      0x000019, 0x00001C, 0x000026, 0x000031, 0x00003B, 0x000046, 0x0C5F52, 0x19BE5F };
CRGBPalette16 pacifica_palette_3 = 
    { 0x000208, 0x00030E, 0x000514, 0x00061A, 0x000820, 0x000927, 0x000B2D, 0x000C33, 
      0x000E39, 0x001040, 0x001450, 0x001860, 0x001C70, 0x002080, 0x1040BF, 0x2060FF };


// Add one layer of waves into the led array
void pacifica_one_layer( CRGBPalette16& p, uint16_t cistart, uint16_t wavescale, uint8_t bri, uint16_t ioff)
{
  uint16_t ci = cistart;
  uint16_t waveangle = ioff;
  uint16_t wavescale_half = (wavescale / 2) + 20;
  for( uint16_t i = 0; i < NUM_LEDS; i++) {
    waveangle += 250;
    uint16_t s16 = sin16( waveangle ) + 32768;
    uint16_t cs = scale16( s16 , wavescale_half ) + wavescale_half;
    ci += cs;
    uint16_t sindex16 = sin16( ci) + 32768;
    uint8_t sindex8 = scale16( sindex16, 240);
    CRGB c = ColorFromPalette( p, sindex8, bri, LINEARBLEND);
    leds[i] += c;
  }
}

// Add extra 'white' to areas where the four layers of light have lined up brightly
void pacifica_add_whitecaps()
{
  uint8_t basethreshold = beatsin8( 9, 55, 65);
  uint8_t wave = beat8( 7 );
  
  for( uint16_t i = 0; i < NUM_LEDS; i++) {
    uint8_t threshold = scale8( sin8( wave), 20) + basethreshold;
    wave += 7;
    uint8_t l = leds[i].getAverageLight();
    if( l > threshold) {
      uint8_t overage = l - threshold;
      uint8_t overage2 = qadd8( overage, overage);
      leds[i] += CRGB( overage, overage2, qadd8( overage2, overage2));
    }
  }
}

// Deepen the blues and greens
void pacifica_deepen_colors()
{
  for( uint16_t i = 0; i < NUM_LEDS; i++) {
    leds[i].blue = scale8( leds[i].blue,  145); 
    leds[i].green= scale8( leds[i].green, 200); 
    leds[i] |= CRGB( 2, 5, 7);
  }
}


void pacifica_loop()
{
  // Increment the four "color index start" counters, one for each wave layer.
  // Each is incremented at a different speed, and the speeds vary over time.
  static uint16_t sCIStart1, sCIStart2, sCIStart3, sCIStart4;
  static uint32_t sLastms = 0;
  uint32_t ms = GET_MILLIS();
  uint32_t deltams = ms - sLastms;
  sLastms = ms;
  uint16_t speedfactor1 = beatsin16(3, 179, 269);
  uint16_t speedfactor2 = beatsin16(4, 179, 269);
  uint32_t deltams1 = (deltams * speedfactor1) / 256;
  uint32_t deltams2 = (deltams * speedfactor2) / 256;
  uint32_t deltams21 = (deltams1 + deltams2) / 2;
  sCIStart1 += (deltams1 * beatsin88(1011,10,13));
  sCIStart2 -= (deltams21 * beatsin88(777,8,11));
  sCIStart3 -= (deltams1 * beatsin88(501,5,7));
  sCIStart4 -= (deltams2 * beatsin88(257,4,6));

  // Clear out the LED array to a dim background blue-green
  fill_solid( leds, NUM_LEDS, CRGB( 2, 6, 10));

  // Render each of four layers, with different scales and speeds, that vary over time
  pacifica_one_layer( pacifica_palette_1, sCIStart1, beatsin16( 3, 11 * 256, 14 * 256), beatsin8( 10, 70, 130), 0-beat16( 301) );
  pacifica_one_layer( pacifica_palette_2, sCIStart2, beatsin16( 4,  6 * 256,  9 * 256), beatsin8( 17, 40,  80), beat16( 401) );
  pacifica_one_layer( pacifica_palette_3, sCIStart3, 6 * 256, beatsin8( 9, 10,38), 0-beat16(503));
  pacifica_one_layer( pacifica_palette_3, sCIStart4, 5 * 256, beatsin8( 8, 10,28), beat16(601));

  // Add brighter 'whitecaps' where the waves lines up more
  pacifica_add_whitecaps();

  // Deepen the blues and greens a bit
  pacifica_deepen_colors();
}

void white(){
  for(int i = 0; i < NUM_LEDS; i++){
    leds[i] = CRGB::White;
  }
}


void Fire2012WithPalette()
{
// Array of temperature readings at each simulation cell
  static uint8_t heat[NUM_LEDS];

  // Step 1.  Cool down every cell a little
    for( int i = 0; i < NUM_LEDS; i++) {
      heat[i] = qsub8( heat[i],  random8(0, ((COOLING * 10) / NUM_LEDS) + 2));
    }
  
    // Step 2.  Heat from each cell drifts 'up' and diffuses a little
    for( int k= NUM_LEDS - 1; k >= 2; k--) {
      heat[k] = (heat[k - 1] + heat[k - 2] + heat[k - 2] ) / 3;
    }
    
    // Step 3.  Randomly ignite new 'sparks' of heat near the bottom
    if( random8() < SPARKING ) {
      int y = random8(7);
      heat[y] = qadd8( heat[y], random8(160,255) );
    }

    // Step 4.  Map from heat cells to LED colors
    for( int j = 0; j < NUM_LEDS; j++) {
      // Scale the heat value from 0-255 down to 0-240
      // for best results with color palettes.
      uint8_t colorindex = scale8( heat[j], 240);
      CRGB color = ColorFromPalette( gPal, colorindex);
      int pixelnumber;
      if( gReverseDirection ) {
        pixelnumber = (NUM_LEDS-1) - j;
      } else {
        pixelnumber = j;
      }
      leds[pixelnumber] = color;
    }
}


void controlLeds(){
  if(ledSt[0] != 0){
    if(!ledsOn){
      digitalWrite(R_PIN, HIGH);
      delay(200);
      FastLED.clear();
      ledsOn = true;
    }
    switch (ledSt[2]){
      case 0:
        pacifica_loop();
      break;

      case 1:
        white();
      break;

      default:
        gPal = HeatColors_p;
        random16_add_entropy( random());
        Fire2012WithPalette();
    }
    FastLED.setBrightness(ledSt[1]);
    FastLED.show();
  } else {
    if(ledsOn){
      FastLED.clear();
      digitalWrite(R_PIN, LOW);
      ledsOn = false;
    }
  }
}

void setup() {
  pinMode(R_PIN, OUTPUT);
  digitalWrite(R_PIN, LOW);
  if (!SPIFFS.begin(true)) {
    ESP.restart();
  }
  Serial2.begin(115200, SERIAL_8N1, 16, 17);    
  while (!Serial2) {};
  delay(100);
  UART.setSerialPort(&Serial2);
  String device_name = "Okhsunrog's esk8 " + String(VERSION);
  NimBLEDevice::init(std::string(device_name.c_str()));
  NimBLEDevice::setMTU(MTU_SIZE);
  NimBLEDevice::setPower(ESP_PWR_LVL_P9);
  sServer = NimBLEDevice::createServer();
  sServer->setCallbacks(new ServerCallbacks()); 
  NimBLEService *rService = sServer->createService(REMOTE_SERVICE_UUID);
  NimBLEService *vService = sServer->createService(VESC_SERVICE_UUID);
  pCharVESCTX = vService->createCharacteristic(VESC_CHARACTERISTIC_UUID_TX,
                                                                NIMBLE_PROPERTY::NOTIFY |
                                                                NIMBLE_PROPERTY::READ);
  pCharVESCRX = vService->createCharacteristic(VESC_CHARACTERISTIC_UUID_RX,
                                                                NIMBLE_PROPERTY::WRITE);
  pCharVESCRX->setCallbacks(new MyCallbacks);
  pCharThrottle = rService->createCharacteristic(THROTTLE_CHARACTERISTIC_UUID,
                                                                NIMBLE_PROPERTY::WRITE);
  pCharThrottle->setCallbacks(new MyCallbacks);
  pCharLD = rService->createCharacteristic(LD_CHARACTERISTIC_UUID,
                                                                NIMBLE_PROPERTY::NOTIFY |
                                                                NIMBLE_PROPERTY::READ);
  pCharLED = rService->createCharacteristic(LED_CHAR_UUID);
  pCharLED->setCallbacks(new MyCallbacks);
  ledSt[0] = 0;
  ledSt[1] = LED_DEFAULT_BR;
  ledSt[2] = 0;
  pCharLED->setValue(ledSt);
  NimBLEService *otaService = sServer->createService(OTA_SERVICE_UUID);
  pCharOTATX = otaService->createCharacteristic(OTA_CHARACTERISTIC_UUID_TX, NIMBLE_PROPERTY::NOTIFY | NIMBLE_PROPERTY::READ );
  pCharOTARX = otaService->createCharacteristic(OTA_CHARACTERISTIC_UUID_RX, NIMBLE_PROPERTY::WRITE | NIMBLE_PROPERTY::READ | NIMBLE_PROPERTY::WRITE_NR);
  pCharOTARX->setCallbacks(new OtaCallbacks());
  vService->start();
  rService->start();
  otaService->start();
  sServer->start();
  //sServer->advertiseOnDisconnect(true);
  NimBLEAdvertising *pAdv = NimBLEDevice::getAdvertising();
  pAdv->addServiceUUID(VESC_SERVICE_UUID);
  pAdv->setScanResponse(true);
  pAdv->setMinPreferred(0x12);
  pAdv->start();
  //esp_task_wdt_init(WDT_TIMEOUT, true); //enable panic so ESP32 restarts
  //esp_task_wdt_add(NULL); //add current thread to WDT watch
  //delay(300);
  //FastLED.addLeds<NEOPIXEL, WS_PIN>(leds, NUM_LEDS);
  FastLED.addLeds<WS2815, WS_PIN>(leds, NUM_LEDS);
  FastLED.setDither(0);
  FastLED.setBrightness(ledSt[1]);
  ldTimer.setTimeOutTime(200);
  ldTimer.reset();
  ledTimer.setTimeOutTime(1000/FRAMES_PER_SECOND);
  ldTimer.reset();
}

void loop() {
  //esp_task_wdt_reset();

  switch (MODE) {

    case NORMAL_MODE:
      if (sServer->getConnectedCount()) {
        if (sendMode) {
          uint8_t fMode[] = {0xAA, false};
          pCharOTATX->setValue(fMode, 2);
          pCharOTATX->notify();
          delay(50);
          sendMode = false;
        }
        if (sendSize) {
          unsigned long x = SPIFFS.totalBytes();
          unsigned long y = SPIFFS.usedBytes();
          uint8_t fSize[] = {0xEF, (uint8_t) (x >> 16), (uint8_t) (x >> 8), (uint8_t) x, (uint8_t) (y >> 16), (uint8_t) (y >> 8), (uint8_t) y};
          pCharOTATX->setValue(fSize, 7);
          pCharOTATX->notify();
          delay(50);
          sendSize = false;
        }

           if(Serial2.available()){
            int t;
            while(Serial2.available()){
                t = Serial2.read();
                bstr.push_back(t);
            }
                while (bstr.length() > 0) {
                    if (bstr.length() > BLE_PACKET_SIZE) {
                        pCharVESCTX->setValue(bstr.substr(0, BLE_PACKET_SIZE));
                        bstr = bstr.substr(BLE_PACKET_SIZE);
                    } else {
                        pCharVESCTX->setValue(bstr);
                        bstr.clear();
                    }
                    pCharVESCTX->notify();
                    delay(10);
                }
          }

        if (ldTimer.hasTimedOut()) {
          updateLD();
          ldTimer.reset();
        }
      }
      if (ledTimer.hasTimedOut()) {
        controlLeds();
        ledTimer.reset();
      }
      break;

    case UPDATE_MODE:

      if (request) {
        uint8_t rq[] = {0xF1, (cur + 1) / 256, (cur + 1) % 256};
        pCharOTATX->setValue(rq, 3);
        pCharOTATX->notify();
        delay(50);
        request = false;
      }

      if (writeFile) {
        if (!current) {
          writeBinary(SPIFFS, "/update.bin", updater, writeLen);
        } else {
          writeBinary(SPIFFS, "/update.bin", updater2, writeLen2);
        }
        writeFile = false;
      }

      if (cur + 1 == parts) { // received complete file
        uint8_t com[] = {0xF2, (cur + 1) / 256, (cur + 1) % 256};
        pCharOTATX->setValue(com, 3);
        pCharOTATX->notify();
        delay(50);
        MODE = OTA_MODE;
      }

      break;

    case OTA_MODE:
      if (writeFile) {
        if (!current) {
          writeBinary(SPIFFS, "/update.bin", updater, writeLen);
        } else {
          writeBinary(SPIFFS, "/update.bin", updater2, writeLen2);
        }
      }


      if (rParts == tParts) {
        delay(1000);
        updateFromFS();
      } else {
        writeFile = true;
        delay(500);
      }
      break;

  }

}